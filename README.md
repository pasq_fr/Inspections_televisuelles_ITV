# Inspections_televisuelles_ITV

Ces fichiers regroupent le travail personnel sur les ITV, j'ai mis ici les fichiers initiés en 2008-2009 lorsque j'étais chargé du visionnage des ITV.
J'ai fait évoluer ces fichiers avec la dernière version que récemment. Notamment pour accompagner le [projet 3liz](https://docs.3liz.org/qgis_drain_sewer_visual_inspection/)

Les codages, bien qu'issus de la norme, sont un **travail de resaisie et de combinaison dans un tableur**. Je sais que ce montage n'est pas idéal ou optimisé, mais à l'époque, je ne travaillais pas en base de données, j'ai donc gardé le principe pour l'instant.

Les descriptions sont des résumés qui ne répondent donc pas à complétement aux descriptions de la norme. Je vous invite à vous référer à la norme pour toutes démarches officielles ou commerciales.

Je vous remercie de me faire part de toutes erreurs ou manquements.

- fichier .csv : contient toutes les combinaisons de code
- fichier .sql : permet la création d'une table postgresql, il est à modifier suivant vos propres besoins (voir dans le texte du fichier)
- fichier sql pour méoire sur la façon d'interroger la table obs du module sewer_visual_inspection

Dans nostalgie : le fichier d'origine retouché en 2015 (*un bon vieux excel à macros*), base du travail.
