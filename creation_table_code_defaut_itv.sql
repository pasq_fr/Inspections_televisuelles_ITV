-- Drop table en cas de besoin
--DROP TABLE code_defaut_itv;

CREATE TABLE code_defaut_itv (
	code varchar(6) NOT NULL,
	descript_code text NULL,
	CONSTRAINT code_defaut_itv_pk PRIMARY KEY (code)
);
CREATE UNIQUE INDEX code_defaut_itv_code_idx ON code_defaut_itv USING btree (code);

COPY code_defaut_itv(code, descript_code)
---remplacer les xxxx par le chemind e votre fichier type c:/dossier/
FROM 'xxxx/codification_ITV_pour_import.csv'
DELIMITER ';'
CSV HEADER;
